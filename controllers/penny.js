/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const request = require('request')
var logger = require('../services/logger')
const moment = require('moment')

var _this = {
    gets: function(req, res){
        var reqData = req.query;
        console.log(reqData)
        console.log(reqData.host+reqData.path)
        request({
            url: reqData.host+reqData.path,
            method: 'GET',
            json: true,
            headers: {
                'User-Agent': 'Robin8 Pipeline',
                accept: 'application/json, text/plain, */*'
            }
        }, (error, response, body) => {
            
            if(error){
                reErr = {
                    error: error
                }
                res.status(400).send(reErr);
                logger.error(JSON.stringify(reErr));
                return 
            }
            var data = response.toJSON();
            res.status(200).send(data);
        });
    }
};


module.exports = _this;

