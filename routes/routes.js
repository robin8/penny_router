/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var penny = require('../controllers/penny')

var appRouter = function (app) {
    app.options("/*", function(req, res, next){
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type');
        res.header('Content-Type', 'application/json; charset=utf-8');
        res.send(200);
    });
    
    app.get("/penny", penny.gets);
}

module.exports = appRouter;