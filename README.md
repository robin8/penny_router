# Robin8 Penny Router

DESC

This project was [NodeJS](https://nodejs.org/) version 8.11.2 and [apiDocs](http://apidocjs.com).

## Development

Start the development run below command:
```
$ git clone https://[username]@bitbucket.org/robin8/penny_router.git
$ cd penny_router
$ npm install
$ node app.js
```

after run `node app.js` the api service is ready at `http://localhost:2088/`.
