/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require('dotenv').load();
var express = require("express");
var bodyParser = require("body-parser");
var routes = require("./routes/routes.js");
var app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use('/test', express.static(__dirname + '/test'))
routes(app);

var server = app.listen(process.env.localport, function () {
    console.log("app running on port.", server.address().port);
});